 <!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>DGN#FOG</title>

		<!-- JQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

		<!-- Bootstrap -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!-- Font-Awesome -->
		<link rel="stylesheet" href="resources/vendor/font-awesome/css/font-awesome.min.css">

		<!-- Isotope -->
		<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

		<!-- Tagsinput http://nicolasbize.com/magicsuggest/-->
		<link rel="stylesheet" href="resources/vendor/tags/magicsuggest.css" />
		<script src="resources/vendor/tags/magicsuggest.js"></script>

		<!-- Google Icons Webfont -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- PHP-Less-Compiler -->
		<?php
			require "resources/vendor/lessphp/lessc.inc.php";
			$less = new lessc;
			$less->checkedCompile("resources/less/default.less", "resources/css/default.css");
		?>
		<link rel="stylesheet" href="resources/css/default.css" />

		<script src="resources/js/default.js"></script>

	</head>

	<body>
		<?php
			if(!empty($_GET) && isset($_GET['login']) && !empty($_GET['login'])){
				$login = true;
				$pencil = '<i class="fa fa-pencil pull-right"></i><i class="fa fa-close pul-right hidden"></i>';
			}else{
				$login = false;
				$pencil = '';
			}
			if(!empty($_GET) && isset($_GET['set']) && !empty($_GET['set'])){
				$set = true;
			}else{
				$set = false;
			}
		?>
		<div id="left">
			<div id="logo"></div>
			<div id="panel">
				<div id="topnavi">
					<?php
						if(!$login):
					?>
					<ul>
						<li><a href="maps.php?login=1" class="login">log in</a></li>
						<li><a href="#">register</a></li>
					</ul>
					<?php
						else:
						$profileImg = './resources/img/profile/profile.jpg';
					?>
					<div id="profile">
						<div id="profileImage" style="background-image: url(<?php echo $profileImg; ?>)"></div>
						<div id="profileRight">
							<div id="profileName">Chris P. Bacon</div>
							<div id="profileLinks">
								<a href="#">Settings</a> |
								<a href="index.php">Sign out</a>
							</div>
						</div>
					</div>
					<?php
						endif;
					?>
				</div>
				<div id="navi" class="block">
					<?php
						if ($login):
					?>
					<div id="filter">
						<ul>
							<li><a href="#">all</a></li>
							<li><a href="#" class="active">my own</a></li>
						</ul>
					</div>
					<?php
						endif;
					?>
					<div id="navigation">
						<ul>
							<li><a href="index.php" class="home active"><span></span></a></li>
							<li><a href="maps.php">maps</a></li>
							<li><a href="#">props</a></li>
							<li><a href="#">textures</a></li>
							<li><a href="#">pictures</a></li>
							<li><a href="#">creatures</a></li>
							<li><a href="#">npcs</a></li>
							<li><a href="#">players</a></li>
							<li><a href="#">market</a></li>
						</ul>
					</div>
				</div>
				<div id="searchbox" class="block">
					<div id="searchfield">

							<input type="text" id="searchInput" name="searchtags"/>
						<i class="fa fa-search"></i>

					</div>
					<div id="keylist">
					</div>
				</div>
				<div id="breadcrumb" class="block">
					<ul>
						<?php
							if(!$login):
						?>
						<li class="current">home</li>
						<?php
							else:
						?>
						<li class="current">my maps</li>
						<?php
							endif;
						?>
					</ul>
				</div>
				<div id="footernavi">
					<ul>
						<li><a href="#">about</a></li>
						<li><a href="#">tour</a></li>
						<li><a href="#">pricing</a></li>
						<li><a href="#">blog</a></li>
						<li><a href="#">help</a></li>
					</ul>
				</div>
				<div id="copyright">&copy; Dungeon Fog 2017</div>
				<div id="infolinks">
					<ul>
						<li><a href="#">Imprint</a></li>
						<li>|</li>
						<li><a href="#">Terms & Conditions</a></li>
						<li>|</li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
				<div id="info">Dungeon Fog is a registered Trademark</div>
			</div>
		</div>
		<div id="main">
			<div id="mainInner" class="content">
				<div class="videoContent"></div>
				<div class="textContent large">
					<p>Welcome to <span class="yellow">DUNGEON</span>FOG where you can <strong>create</strong>, <strong>organize</strong> and <strong>share</strong> all your preciouse game maps.</p>
				</div>
				<div class="boxContent">
					<div class="box">
						<div class="boxImage">
							<img src="" />
						</div>
						<div class="boxText">
							<p>Draw <strong>buildings, rooms, dungeons, outdoor sets</strong> in many different settings like <strong>fantasy, sci-fi, dystopies</strong> and many more.</p>
						</div>
						<div clasS="boxLink">
							<a href="#">read more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
