$(document).ready(function(){
	/* init Masonry Bricks ----------------------------------------------------- */
		var $grid = $('#mainInner.maps').isotope({
			// options
			itemSelector: '.brick-item',
			//layoutMode: 'packery',
			columnWidth: '.brick',
			getSortData: {
				type: '[data-category]'
			},
			sortBy : [ 'type' ]
		});
		var $stamp = $grid.find('.stamp');
		$grid.isotope('stamp', $stamp);
		$grid.isotope('layout');
	/* ------------------------------------------------------------------------- */

	/* init Magic Suggest ------------------------------------------------------ */
		$('#searchInput').magicSuggest({
			placeholder: '',
			maxSelection: 20,
			data: ['da', 'k&ouml;nnte', 'man', 'schon', 'tags', 'vorschlagen', 'oder', 'die', 'auswahl', 'gleich', 'auf', 'related', 'tags', 'einschr&auml;nken']
		});
	/* ------------------------------------------------------------------------- */

	/* Edit MapSet/Map Names & Description ------------------------------------- */
		//$('.mapSetName i, .mapTitle i').bind('click', function(){
		$(document).on('click', '.mapSetName i, .mapTitle i, .mapDetailName i, .mapDetailsDescriptionText i', function(){
			switch($(this).parent().attr('class').replace(' pr', '')){
				case 'mapSetName':
				case 'mapTitle':
					editContent($(this), 'a', 'input', false);
					break;
				case 'mapDetailName':
					editContent($(this), 'span', 'input', true);
					break;
				case 'mapDetailsDescriptionText':
					editContent($(this), 'div', 'textarea', false);
					break;
			}
		});
	/* ------------------------------------------------------------------------- */

	/* Edit Tags --------------------------------------------------------------- */
		$(document).on('click', '.mapDetailsDescriptionTags i', function(){
			var val = $(this).parent().find('.mapDetailsDescriptionTagsInner').text();
			var saved = false;

			if ($(this).hasClass('fa-check')) {
				$(this).parent().find('.fa-check').toggleClass('fa-pencil fa-check');
				var newVal = [];
				$(this).parent().find('.ms-sel-item ').each(function(){
					newVal.push('<a href="#">#'+$(this).text()+'</a>');
				});
				$(this).parent().find('#tagsInput').remove();
				$(this).parent().find('div').html(newVal.join(', ')).show();
				$(this).parent().find('i.fa-close').toggleClass('hidden');
				saved = true;
			}
			if ($(this).hasClass('fa-close') && !saved) {
				$(this).parent().find('.fa-check').toggleClass('fa-pencil fa-check');
				$(this).parent().find('#tagsInput').remove();
				$(this).parent().find('div').show();
				$(this).toggleClass('hidden');
			}
			if ($(this).hasClass('fa-pencil') && !saved) {
				$(this).parent().find('div').hide();

				$(this).parent().append('<input id="tagsInput" name="tagsInput">');

				valArr = $.trim(val).replace(/\#/g, '').split(', ');

				$(this).parent().find('input').magicSuggest({
					placeholder: '',
					maxSelection: 20,
					value: valArr,
					data: ['da', 'k&ouml;nnte', 'man', 'schon', 'tags', 'vorschlagen', 'oder', 'die', 'auswahl', 'gleich', 'auf', 'related', 'tags', 'einschr&auml;nken']
				});

				$(this).parent().find('i.fa-close').toggleClass('hidden');
				$(this).toggleClass('fa-pencil fa-check');
			}
		});
	/* ------------------------------------------------------------------------- */

	/* Create new Folder ------------------------------------------------------- */
		$('#saveFolder').on('click', function(){
			//.prepend f�r maps
			//append f�r mapSets
			var container = `<div class="mapSet brick brick-item" data-category="a">
								<div class="brickInner">
									<div class="mapSetImages">
										<div class="mapSetInfo thumb-xlg">
											<div class="mapSetInfoInner">this set is empty</div>
												<a href="#" data-toggle="modal" data-target="#createMap">+ new map</a>
											</div>
										</div>
										<div class="mapSetName">
											<a href="#">`+$(this).parent().find('input').val()+`</a>
											<i class="fa fa-pencil pull-right"></i>
											<i class="fa fa-close pul-right hidden"></i>
										</div>
									<div class="mapSetMove">
										<select name="mapSetMove_id-1">
											<option value="">move to</option>
											<option value="">any</option>
											<option value="">directory</option>
											<option value="">that</option>
											<option value="">has</option>
											<option value="">been</option>
											<option value="">created</option>
											<option value="">so</option>
											<option value="">far</option>
										</select>
									</div>
									<div class="mapSetActions clearfix">
										<div class="mapSetPublic active pull-left">
											<span>Public</span>
											<i class="fa fa-check-square-o"></i>
										</div>
										<div class="mapSetDelete pull-right">
											<i class="fa fa-trash"></i>
										</div>
									</div>
								</div>
							</div>`;

			$content = $( container );
			//$grid.prepend( $content ).isotope( 'prepended', $content );
			$grid.append( $content ).isotope( 'appended', $content ).isotope({sortBy: 'type'}).isotope();
		});
	/* ------------------------------------------------------------------------- */

	/* Map Detail - DM Notes functionality ------------------------------------- */
		//show/hide room content
		$('.dmn_room_head').on('click', function(){
			$(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
			if ($(this).next('.dmn_room_content').is(':visible')) {
				//$(this).next('.dmn_room_content').slideUp();
				$(this).next('.dmn_room_content').hide();
			}else{
				//$(this).next('.dmn_room_content').slideDown();
				$(this).next('.dmn_room_content').show();
			}
		});

		////reveal toggle
		//$('.dm_notes_room_reveal').on('click', function(){
		//	$(this).toggleClass('reveal revealed');
		//});
        //
		////show/hide toggle
		//$('.dm_notes_room_show').on('click', function(){
		//	$(this).toggleClass('vis invis');
		//});
	/* ------------------------------------------------------------------------- */

	/* Toggle Set/Map public --------------------------------------------------- */
		//$('.mapSetPublic').on('click', function(){
		$(document).on('click', '.mapSetPublic, .mapDetailPublic', function(){
			$(this).find('i').toggleClass('fa-check-square-o fa-square-o');
			$(this).toggleClass('active');
		});
	/* ------------------------------------------------------------------------- */

	/* Delete Set/Map ---------------------------------------------------------- */
		$(document).on('click', '.mapSetDelete', function(){
			var item = '';
			var classes = $(this).parents('.brick').attr('class').split(' ');
			var parent = classes[0];

			switch (parent) {
				case 'mapSet':
					item = "map set '"+$.trim($(this).parents('.brick').find('.mapSetName').text())+"' and all its containing maps";
					break;
				case 'map':
					item = "map '"+$.trim($(this).parents('.brick').find('.mapSetName').text())+"'";
					break;
			}
			if(confirm('Would you like to delete the '+item+'?')){
				$(this).parents('.'+parent).remove();
				$grid.isotope({sortBy: 'type'}).isotope();
			}
		});
	/* ------------------------------------------------------------------------- */

	/* FUNCTIONS --------------------------------------------------------------- */
	function editContent(object, selector, input, pr = false) {
		var val = object.parent().find(selector).text();
		var saved = false;
		var inputfield = '';
		switch (input) {
			case 'input':
				inputfield = '<input type="text" value="'+val+'">';
				break;
			case 'textarea':
				var valHeight = object.parent().find(selector).height();
				inputfield = '<textarea name="mapDescr" class="" style="height:'+valHeight+'px; width:100%; resize: vertical;">'+$.trim(val)+'</textarea>'
				break;
			default:
				inputfield = '<input type="text" value="'+val+'">';
		}
		if (object.hasClass('fa-check')) {
			if (pr) {
				object.parent().toggleClass('pr');
			}
			object.parent().find('.fa-check').toggleClass('fa-pencil fa-check');
			var newVal = object.parent().find(input).val();
			object.parent().find(input).remove();
			object.parent().find(selector).text(newVal).show();
			object.parent().find('i.fa-close').toggleClass('hidden');
			saved = true;
		}
		if (object.hasClass('fa-close') && !saved) {
			if (pr) {
				object.parent().toggleClass('pr');
			}
			object.parent().find('.fa-check').toggleClass('fa-pencil fa-check');
			object.parent().find(input).remove();
			object.parent().find(selector).show();
			object.toggleClass('hidden');
		}
		if (object.hasClass('fa-pencil') && !saved) {
			if (pr) {
				object.parent().toggleClass('pr');
			}
			object.parent().find(selector).hide();
			object.parent().append(inputfield);
			object.parent().find('i.fa-close').toggleClass('hidden');
			object.toggleClass('fa-pencil fa-check');
			object.parent().find(input).select();
		}
	}
});
