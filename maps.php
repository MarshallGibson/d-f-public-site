 <!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title>DGN#FOG</title>

		<!-- JQuery -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

		<!-- Bootstrap -->
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!-- Font-Awesome -->
		<link rel="stylesheet" href="resources/vendor/font-awesome/css/font-awesome.min.css">

		<!-- Isotope -->
		<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js"></script>

		<!-- Tagsinput http://nicolasbize.com/magicsuggest/-->
		<link rel="stylesheet" href="resources/vendor/tags/magicsuggest.css" />
		<script src="resources/vendor/tags/magicsuggest.js"></script>

		<!-- Google Icons Webfont -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<!-- PHP-Less-Compiler -->
		<?php
			require "resources/vendor/lessphp/lessc.inc.php";
			$less = new lessc;
			$less->checkedCompile("resources/less/default.less", "resources/css/default.css");
		?>
		<link rel="stylesheet" href="resources/css/default.css" />

		<script src="resources/js/default.js"></script>

	</head>

	<body>
		<?php
			if(!empty($_GET) && isset($_GET['login']) && !empty($_GET['login'])){
				$login = true;
				$pencil = '<i class="fa fa-pencil pull-right"></i><i class="fa fa-close pul-right hidden"></i>';
			}else{
				$login = false;
				$pencil = '';
			}
			if(!empty($_GET) && isset($_GET['set']) && !empty($_GET['set'])){
				$set = true;
			}else{
				$set = false;
			}
		?>
		<div id="left">
			<div id="logo"></div>
			<div id="panel">
				<div id="topnavi">
					<?php
						if(!$login):
					?>
					<ul>
						<li><a href="maps.php?login=1" class="login">log in</a></li>
						<li><a href="#">register</a></li>
					</ul>
					<?php
						else:
						$profileImg = './resources/img/profile/profile.jpg';
					?>
					<div id="profile">
						<div id="profileImage" style="background-image: url(<?php echo $profileImg; ?>)"></div>
						<div id="profileRight">
							<div id="profileName">Chris P. Bacon</div>
							<div id="profileLinks">
								<a href="#">Settings</a> |
								<a href="index.php">Sign out</a>
							</div>
						</div>
					</div>
					<?php
						endif;
					?>
				</div>
				<div id="navi" class="block">
					<?php
						if ($login):
					?>
					<div id="filter">
						<ul>
							<li><a href="#">all</a></li>
							<li><a href="#" class="active">my own</a></li>
						</ul>
					</div>
					<?php
						endif;
					?>
					<div id="navigation">
						<ul>
							<li><a href="index.php" class="home"><span></span></a></li>
							<li><a href="#" class="active">maps</a></li>
							<li><a href="#">props</a></li>
							<li><a href="#">textures</a></li>
							<li><a href="#">pictures</a></li>
							<li><a href="#">creatures</a></li>
							<li><a href="#">npcs</a></li>
							<li><a href="#">players</a></li>
							<li><a href="#">market</a></li>
						</ul>
					</div>
				</div>
				<div id="searchbox" class="block">
					<div id="searchfield">

							<input type="text" id="searchInput" name="searchtags"/>
						<i class="fa fa-search"></i>

					</div>
					<div id="keylist">
						<a href="#">#town</a>, <a href="#">#tavern</a>, <a href="#">#mansion</a>, <a href="#">#fantasy</a>, <a href="#">#CurseOfStrahd</a>
					</div>
				</div>
				<div id="breadcrumb" class="block">
					<ul>
						<?php
							if(!$login):
						?>
						<li><a href="#">my campaigns</a></li>
						<li><i class="fa fa-chevron-right"></i></li>
						<li><a href="#">curse of strahd</a></li>
						<li><i class="fa fa-chevron-right"></i></li>
						<li><a href="#">set</a></li>
						<li><i class="fa fa-chevron-right"></i></li>
						<li class="current">set 2</li>
						<?php
							else:
						?>
						<li class="current">my maps</li>
						<?php
							endif;
						?>
					</ul>
				</div>
				<div id="footernavi">
					<ul>
						<li><a href="#">about</a></li>
						<li><a href="#">tour</a></li>
						<li><a href="#">pricing</a></li>
						<li><a href="#">blog</a></li>
						<li><a href="#">help</a></li>
					</ul>
				</div>
				<div id="copyright">&copy; Dungeon Fog 2017</div>
				<div id="infolinks">
					<ul>
						<li><a href="#">Imprint</a></li>
						<li>|</li>
						<li><a href="#">Terms & Conditions</a></li>
						<li>|</li>
						<li><a href="#">Privacy Policy</a></li>
					</ul>
				</div>
				<div id="info">Dungeon Fog is a registered Trademark</div>
			</div>
		</div>
		<div id="main">
			<div id="mainInner" class="maps">
				<?php
					if($login):
				?>

				<?php
					if($set):
				?>
				<div class="mapSet brick stamp">
					<div class="brickInner">
						<div class="mapSetBack">
							<a href="index.php?login=1">
								<i class="fa fa-chevron-left"></i>
								<span>back</span>
							</a>
						</div>
						<div class="mapSetName"><span>Curse Of Strahd</span><?php echo $pencil; ?></div>
						<?php if($login): ?>
						<div class="mapLaunchEdit clearfix">
							<span class="mapLaunch pull-left"><a href="#">Launch Player</a></span>
						</div>
						<div class="mapSetMove">
							<select name="mapSetMove_id-1">
								<option value="">move to</option>
								<option value="">any</option>
								<option value="">directory</option>
								<option value="">that</option>
								<option value="">has</option>
								<option value="">been</option>
								<option value="">created</option>
								<option value="">so</option>
								<option value="">far</option>
							</select>
						</div>
						<div class="mapSetActions clearfix">
							<div class="mapSetPublic active pull-left"><span>Public</span> <i class="fa fa-check-square-o"></i></div>
							<div class="mapSetDelete pull-right">
								<span>Delete</span><i class="fa fa-trash"></i>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php
					endif;
				?>
				<div class="addWrapper brick stamp">
					<div class="add" data-toggle='modal' data-target='#createMap'>
						<div class="addIcon"><i class="fa fa-edit"></i></div>
						<div class="addText">new map</div>
					</div>
					<div class="add" data-toggle='modal' data-target='#createFolder'>
						<div class="addIcon"><i class="fa fa-folder-open"></i></div>
						<div class="addText">new folder</div>
					</div>
				</div>
				<?php
					endif;
				?>


				<?php
					$mapPath = './resources/img/maps';
					$profileImg = './resources/img/profile/profile.jpg';
					$bricks = array();
					$text = 'Bacon ipsum dolor amet bresaola short loin porchetta rump, kevin shoulder ground round flank tri-tip sirloin capicola pancetta ham hock pastrami pork. Kevin tenderloin ground round leberkas.';
					foreach(scandir($mapPath) as $el){
						if($el != '.' AND $el != '..'){
							$elementPath = $mapPath.'/'.$el;
							//style='background-image:url(\"$elementPath\")'
							$imgProperties = getimagesize($elementPath);

							#echo "<pre>";
							#print_r($imgProperties);
							#echo "</pre>";

							$aspect = 278/$imgProperties[0];
							$aspect = 268/$imgProperties[0];

							#echo "<pre>";
							#print_r($imgProperties[1]*$aspect);
							#echo "</pre>";

							if($login){
								$addition = '
											<div class="mapLaunchEdit clearfix">
												<span class="mapLaunch pull-left"><a href="#">Launch Player</a></span>
												<span class="mapEdit pull-right"><a href="#">Edit</a></span>
											</div>
											<div class="mapSetMove">
												<select name="mapSetMove_id-1">
													<option value="">move to</option>
													<option value="">any</option>
													<option value="">directory</option>
													<option value="">that</option>
													<option value="">has</option>
													<option value="">been</option>
													<option value="">created</option>
													<option value="">so</option>
													<option value="">far</option>
												</select>
											</div>
											<div class="mapSetActions clearfix">
												<div class="mapSetPublic active pull-left"><span>Public</span> <i class="fa fa-check-square-o"></i></div>
												<div class="mapSetDelete pull-right">
													<span>Delete</span><i class="fa fa-trash"></i>
												</div>
											</div>';
							}else{
								$addition = '';
							}

							$bricks[] = "<div class='map brick brick-item' data-category='b'>
											<div class='brickInner'>

												<div class='mapImg' style='background-image:url(\"$elementPath\"); height:".($imgProperties[1]*$aspect)."px;' data-toggle='modal' data-target='#mapDetails'>
													<div class='iconbar'>
														<div class='iconbarInner'>
															<div class='icon-gem icon'></div>
															<div class='icon-plane icon'>
																<div class='icons-social clearfix'>
																	<div class='icon-facebook social-icon'>
																	</div>
																	<div class='icon-twitter social-icon'>
																	</div>
																	<div class='icon-pinterest social-icon'>
																	</div>
																	<div class='icon-google social-icon'>
																	</div>
																</div>
															</div>
															<div class='icon-book icon'></div>
															<div class='icon-cloud icon disabled'></div>
															<div class='icon-arrow icon disabled'></div>
														</div>
													</div>
												</div>
												<div class='owner'>
													<div class='ownerName'>Chris P. Bacon</div>
													<div class='ownerImg' style='background-image:url(\"$profileImg\")'></div>
												</div>
												<div class='mapDescription'>
													<div class='mapTitle'>
														<a href='#' data-toggle='modal' data-target='#mapDetails'>Town's Tavern</a>$pencil
													</div>
													<div class='mapText'>
														".substr($text, 0, rand(strlen($text)/4, strlen($text)))."
													</div>
													<div class='mapTags'>
														<a href='#'>#town</a>, <a href='#'>#tavern</a>, <a href='#'>#mansion</a>, <a href='#'>#fantasy</a>, <a href='#'>#CurseOfStrahd</a>
													</div>
													<div class='mapInfo'>
														liked: 1.234 | shared: 456 | bookmarked: 678
													</div>
												</div>
												$addition
											</div>
										</div>";
						}
					}

					for($i = 0; $i < 10; $i++){
						shuffle($bricks);
						echo implode('', $bricks);
					}
				?>

				<div class="mapSet brick brick-item" data-category='a'>
					<div class="brickInner">
						<div class="mapSetImages">
							<?php
								$mapCount = 1;
								$mapPath = './resources/img/maps';
								$mapSetInfoClass = 'thumb-sm';
								if($mapCount == 0){
									$thumbs = '';
									$mapSetInfoClass = 'thumb-xlg';
								}elseif($mapCount == 1){
									$elPath1 = $mapPath.'/map1.png';

									$thumbs = "<div class='mapSetThumb thumb-sm first' style='background-image: url(\"$elPath1\")'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm'></div>";
								}elseif($mapCount == 2){
									$elPath1 = $mapPath.'/map1.png';
									$elPath2 = $mapPath.'/map2.png';

									$thumbs = "<div class='mapSetThumb thumb-sm first' style='background-image: url(\"$elPath1\")'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm' style='background-image: url(\"$elPath2\")'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm'></div>";
								}elseif($mapCount >= 3){
									$elPath1 = $mapPath.'/map1.png';
									$elPath2 = $mapPath.'/map2.png';
									$elPath3 = $mapPath.'/map3.png';

									$thumbs = "<div class='mapSetThumb thumb-sm first' style='background-image: url(\"$elPath1\")'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm' style='background-image: url(\"$elPath2\")'></div>";
									$thumbs .= "<div class='mapSetThumb thumb-sm' style='background-image: url(\"$elPath3\")'></div>";
								}

								echo $thumbs;
							?>
							<div class="mapSetInfo <?php echo $mapSetInfoClass; ?>">
								<div class="mapSetInfoInner">
									contains <span class="mapSetCount">5 sets</span> and <span class="mapSetCount">1k+ maps</span>
								</div>
								<?php
									if($login){
										echo '<a href="#" data-toggle="modal" data-target="#createMap">+ new map</a>';
									}
								?>
							</div>
						</div>
						<div class="mapSetName"><a href="index.php?set=1<?php echo ($login) ? '&login=1' : ''; ?>">Curse Of Strahd</a><?php echo $pencil; ?></div>
						<?php if($login): ?>
						<div class="mapLaunchEdit clearfix">
							<span class="mapLaunch pull-left"><a href="#">Launch Player</a></span>
						</div>
						<div class="mapSetMove">
							<select name="mapSetMove_id-1">
								<option value="">move to</option>
								<option value="">any</option>
								<option value="">directory</option>
								<option value="">that</option>
								<option value="">has</option>
								<option value="">been</option>
								<option value="">created</option>
								<option value="">so</option>
								<option value="">far</option>
							</select>
						</div>
						<div class="mapSetActions clearfix">
							<div class="mapSetPublic active pull-left"><span>Public</span> <i class="fa fa-check-square-o"></i></div>
							<div class="mapSetDelete pull-right">
								<span>Delete</span><i class="fa fa-trash"></i>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div id="mapDetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mapDetails">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<div class="mapDetailName">
							<span>Town's Tavern</span>
							<?php echo $pencil ?>
						</div>
						<div class="mapDetailOwner">
							<div class="mapDetailOwnerName">Chris P. Bacon</div>
							<div class="mapDetailOwnerImage" style="background-image:url('./resources/img/profile/profile.jpg')"></div>
						</div>
					</div>
					<div class="modal-body">
						<div class="mapDetailImage">
							<?php
								$img = './resources/img/maps/map1.png';
								$imgSize = getimagesize($img);

								//600 = ModalContainer Breite - 2 = Border l&r
								$aspect = (600-2)/$imgSize[0];
							?>
							<div class="mapDetailImageInner" style="background-image:url(<?php echo $img; ?>); <?php echo "height: ".($imgSize[1]*$aspect)."px" ?>">
								<div class='iconbar'>
									<div class='iconbarInner'>
										<div class='icon-gem icon'></div>
										<div class='icon-plane icon'>
											<div class='icons-social clearfix'>
												<div class='icon-facebook social-icon'>
												</div>
												<div class='icon-twitter social-icon'>
												</div>
												<div class='icon-pinterest social-icon'>
												</div>
												<div class='icon-google social-icon'>
												</div>
											</div>
										</div>
										<div class='icon-book icon'></div>
										<div class='icon-cloud icon disabled'></div>
										<div class='icon-arrow icon disabled'></div>
									</div>
								</div>
							</div>
						</div>
						<div class="mapDetailDescription">
							<div class="mapDetailsDescriptionLeft">
								<?php if ($login): ?>
								<div class="mapDetailsDescriptionLeftTop">
									<div class="mapLaunchEdit clearfix">
										<span class="mapLaunch pull-left"><a href="#">Launch Player</a></span>
										<span class="mapEdit pull-right"><a href="#">Edit</a></span>
									</div>
									<div class="mapDetailMove">
										<select name="mapSetMove_id-1">
											<option value="">move to</option>
											<option value="">any</option>
											<option value="">directory</option>
											<option value="">that</option>
											<option value="">has</option>
											<option value="">been</option>
											<option value="">created</option>
											<option value="">so</option>
											<option value="">far</option>
										</select>
									</div>
									<div class="mapDetailActions clearfix">
										<div class="mapDetailPublic active pull-left"><span>Public</span> <i class="fa fa-check-square-o"></i></div>
										<div class="mapDetailDelete pull-right">
											<span>Delete</span><i class="fa fa-trash"></i>
										</div>
									</div>
								</div>
								<?php endif; ?>
								<div class="mapDetailsDescriptionLeftBottom">
									<div class="mapDetailsDescriptionText">
										<?php echo $pencil ?>
										<div class="mapDetailsDescriptionTextInner <?php echo ($pencil != '') ? 'pr' : ''; ?>">
											Bacon ipsum dolor amet bresaola short loin porchetta rump, kevin shoulder ground round flank tri-tip sirloin capicola pancetta ham hock.
										</div>
									</div>
									<div class="mapDetailsDescriptionTags">
										<?php echo $pencil ?>
										<div class="mapDetailsDescriptionTagsInner <?php echo ($pencil != '') ? 'pr' : ''; ?>">
											<a href='#'>#town</a>, <a href='#'>#tavern</a>, <a href='#'>#mansion</a>, <a href='#'>#fantasy</a>, <a href='#'>#CurseOfStrahd</a>
										</div>
									</div>
								</div>
							</div>
							<div class="mapDetailsDescriptionRight">
								<div class="mapDetailsDescriptionDMNotes">
									<div class="dmn_descr_general">
										Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.<br/><br/>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
									</div>
									<div class="dmn_rooms">
										<div class="dmn_room">
											<div class="dmn_room_head">
												<span class="dmn_room_nr">1</span>
												<span class="dmn_room_name">Entrance</span>
												<i class="fa fa-chevron-down pull-right"></i>
											</div>
											<div class="dmn_room_content">
												<div class="dmn_quote">
													Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
													<br/>
													<br/>
													Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
												</div>
												<div class="dmn_room_details underlay">
													<span class="icon icon-room"></span>
													<div class="dmn_descr">
														The walls are made of old cobblestone. The floor is made of old wooden planks. Two doors are leading into this room.
													</div>
												</div>
												<div class="dmn_room_props underlay">
													<span class="icon icon-props"></span>
													<div class="dmn_descr">
														The room contains:<br/>
														4 tables, 1 broken table, 8 chairs, 1 bar, 6 bar stools, 4 drawers, 1 rug and 1 wallmounted bearhead
													</div>
												</div>
												<div class="dmn_room_doors underlay">
													<span class="icon icon-doors"></span>
													<div class="dmn_descr">
														1 door is made out of wood, is unlocked and untrapped
														<br/>
														<br/>
														1 door is locked (DC 15) and trapped (detect DC 17, disarm DC 20).<br/>
														Note: arrow deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_traps underlay">
													<span class="icon icon-traps"></span>
													<div class="dmn_descr">
														There are 2 hidden traps in this room.
														<br/>
														<br/>
														Pit trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind cellar door+++ deals 1d6 fall damage (halfed on save)
														<br/>
														<br/>
														Flying darts trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind bar+++ deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_images clearfix">
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/jennek.jpg">
														</div>
														<div class="dmn_img_name">Barkeeper Jennek</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/sandra.jpg">
														</div>
														<div class="dmn_img_name">Barmaid Sandra</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/guhl.jpg">
														</div>
														<div class="dmn_img_name">Guhl</div>
													</div>
												</div>
												<div class="dmn_room_additional">
													<div class="dmn_subtitle">Meals & Drinks</div>
													<div class="dmn_table">
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Prok Knuckle Roasted in Beer</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Braised Oxtails with Mushrooms</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Chicken Liver Pate and Bread</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Grilled Fillet of Salmon</td>
																	<td>7 sp</td>
																</tr>
																<tr>
																	<td>Spiced Potatoes</td>
																	<td>7 sp</td>
																</tr>
															</tbody>
														</table>
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Whiskey</td>
																	<td>9 cp</td>
																</tr>
																<tr>
																	<td>Jenneck's Wine</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Pale Ale</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Dark Ale</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Milk</td>
																	<td>3 cp</td>
																</tr>
															</tbody>
														</table>
													
													</div>
												</div>
											</div>
										</div>
										<div class="dmn_room">
											<div class="dmn_room_head">
												<span class="dmn_room_nr">2</span>
												<span class="dmn_room_name">Bar</span>
												<i class="fa fa-chevron-down pull-right"></i>
											</div>
											<div class="dmn_room_content">
												<div class="dmn_quote">
													Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
													<br/>
													<br/>
													Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
												</div>
												<div class="dmn_room_details underlay">
													<span class="icon icon-room"></span>
													<div class="dmn_descr">
														The walls are made of old cobblestone. The floor is made of old wooden planks. Two doors are leading into this room.
													</div>
												</div>
												<div class="dmn_room_props underlay">
													<span class="icon icon-props"></span>
													<div class="dmn_descr">
														The room contains:<br/>
														4 tables, 1 broken table, 8 chairs, 1 bar, 6 bar stools, 4 drawers, 1 rug and 1 wallmounted bearhead
													</div>
												</div>
												<div class="dmn_room_doors underlay">
													<span class="icon icon-doors"></span>
													<div class="dmn_descr">
														1 door is made out of wood, is unlocked and untrapped
														<br/>
														<br/>
														1 door is locked (DC 15) and trapped (detect DC 17, disarm DC 20).<br/>
														Note: arrow deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_traps underlay">
													<span class="icon icon-traps"></span>
													<div class="dmn_descr">
														There are 2 hidden traps in this room.
														<br/>
														<br/>
														Pit trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind cellar door+++ deals 1d6 fall damage (halfed on save)
														<br/>
														<br/>
														Flying darts trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind bar+++ deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_images clearfix">
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/jennek.jpg">
														</div>
														<div class="dmn_img_name">Barkeeper Jennek</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/sandra.jpg">
														</div>
														<div class="dmn_img_name">Barmaid Sandra</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/guhl.jpg">
														</div>
														<div class="dmn_img_name">Guhl</div>
													</div>
												</div>
												<div class="dmn_room_additional">
													<div class="dmn_subtitle">Meals & Drinks</div>
													<div class="dmn_table">
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Prok Knuckle Roasted in Beer</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Braised Oxtails with Mushrooms</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Chicken Liver Pate and Bread</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Grilled Fillet of Salmon</td>
																	<td>7 sp</td>
																</tr>
																<tr>
																	<td>Spiced Potatoes</td>
																	<td>7 sp</td>
																</tr>
															</tbody>
														</table>
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Whiskey</td>
																	<td>9 cp</td>
																</tr>
																<tr>
																	<td>Jenneck's Wine</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Pale Ale</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Dark Ale</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Milk</td>
																	<td>3 cp</td>
																</tr>
															</tbody>
														</table>
													
													</div>
												</div>
											</div>
										</div>
										<div class="dmn_room">
											<div class="dmn_room_head">
												<span class="dmn_room_nr">3</span>
												<span class="dmn_room_name">Storage</span>
												<i class="fa fa-chevron-down pull-right"></i>
											</div>
											<div class="dmn_room_content">
												<div class="dmn_quote">
													Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.
													<br/>
													<br/>
													Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.
												</div>
												<div class="dmn_room_details underlay">
													<span class="icon icon-room"></span>
													<div class="dmn_descr">
														The walls are made of old cobblestone. The floor is made of old wooden planks. Two doors are leading into this room.
													</div>
												</div>
												<div class="dmn_room_props underlay">
													<span class="icon icon-props"></span>
													<div class="dmn_descr">
														The room contains:<br/>
														4 tables, 1 broken table, 8 chairs, 1 bar, 6 bar stools, 4 drawers, 1 rug and 1 wallmounted bearhead
													</div>
												</div>
												<div class="dmn_room_doors underlay">
													<span class="icon icon-doors"></span>
													<div class="dmn_descr">
														1 door is made out of wood, is unlocked and untrapped
														<br/>
														<br/>
														1 door is locked (DC 15) and trapped (detect DC 17, disarm DC 20).<br/>
														Note: arrow deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_traps underlay">
													<span class="icon icon-traps"></span>
													<div class="dmn_descr">
														There are 2 hidden traps in this room.
														<br/>
														<br/>
														Pit trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind cellar door+++ deals 1d6 fall damage (halfed on save)
														<br/>
														<br/>
														Flying darts trap (detect DC 17, disarm DC 20)
														<br/>
														Note: ++behind bar+++ deals 1d6 piercing damage (halfed on save)
													</div>
												</div>
												<div class="dmn_room_images clearfix">
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/jennek.jpg">
														</div>
														<div class="dmn_img_name">Barkeeper Jennek</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/sandra.jpg">
														</div>
														<div class="dmn_img_name">Barmaid Sandra</div>
													</div>
													<div class="dmn_room_img col-md-6 col-sm-12">
														<div class="dmn_img_wrapper">
															<img src="resources/img/users/c5c1d2c75d39ba4a9b746d07c98f8c53f331f179/guhl.jpg">
														</div>
														<div class="dmn_img_name">Guhl</div>
													</div>
												</div>
												<div class="dmn_room_additional">
													<div class="dmn_subtitle">Meals & Drinks</div>
													<div class="dmn_table">
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Prok Knuckle Roasted in Beer</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Braised Oxtails with Mushrooms</td>
																	<td>6 sp</td>
																</tr>
																<tr>
																	<td>Chicken Liver Pate and Bread</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Grilled Fillet of Salmon</td>
																	<td>7 sp</td>
																</tr>
																<tr>
																	<td>Spiced Potatoes</td>
																	<td>7 sp</td>
																</tr>
															</tbody>
														</table>
														<table class="table table-striped">
															<tbody>
																<tr>
																	<td>Whiskey</td>
																	<td>9 cp</td>
																</tr>
																<tr>
																	<td>Jenneck's Wine</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Pale Ale</td>
																	<td>5 sp</td>
																</tr>
																<tr>
																	<td>Dark Ale</td>
																	<td>6 cp</td>
																</tr>
																<tr>
																	<td>Milk</td>
																	<td>3 cp</td>
																</tr>
															</tbody>
														</table>
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
								if(!$login):
							?>
							<div class="mapDetailsOverlayBG"></div>
							<div class="mapDetailsOverlay">
								<div class="mapDetailsOverlayLeft">
									<div class="mapDetailsOverlayInfo">create your free account</div>
									<div class="mapDetailsOverlayButton">
										<a href="#">Sign up</a>
									</div>
								</div>
								<div class="mapDetailsOverlayRight">
									<div class="mapDetailsOverlayText">
										and get access to all of our features like the DM Notes for each map, more props and textures, high resolution downloads, and many more!
									</div>
								</div>
							</div>
							<?php
								endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="createFolder" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createFolder">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<div class="createFolderHeadline pull-left">Create new folder</div>
					</div>
					<div class="modal-body clearfix">
						<input type="text" name="createFolder" placeholder="New Folder">
						<button id="saveFolder" class="pull-right" data-dismiss="modal">ok</button>
					</div>
				</div>
			</div>
		</div>
		<div id="createMap" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="createMap">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header clearfix">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<div class="createMapHeadline pull-left">Create new map</div>
					</div>
					<div class="modal-body clearfix">
						<input type="text" name="createMap" placeholder="New Map">
						<div class="modalMapProperties">
							<div class="modalMapSize">
								<div class="modalMapPropertiesLabel">Map Size (Grids)</div>
								<div class="modalMapPropertiesWidth">
									<span>width</span><input type="text" name="mapWidth" placeholder="1000">
								</div>
								<div class="modalMapPropertiesHeight">
									<span>height</span><input type="text" name="mapHeight" placeholder="1000">
								</div>
							</div>
							<div class="modalGridSize">
								<div class="modalMapPropertiesLabel">Grid (Pixel)</div>
								<div class="modalMapPropertiesWidth">
									<span>width</span><input type="text" name="gridWidth" placeholder="50">
								</div>
								<div class="modalMapPropertiesHeight">
									<span>height</span><input type="text" name="gridHeight" placeholder="50">
								</div>
							</div>
							<div class="modalGridDistance">
								<div class="modalMapPropertiesLabel">Unit (per Grid)</div>
								<div class="modalMapPropertiesWidth">
									<input type="text" name="unit" placeholder="5">
								</div>
								<div class="modalMapPropertiesHeight">
									<select name="unitMeasure">
										<option>feet</option>
										<option>inches</option>
										<option>miles</option>
										<option>meters</option>
										<option>kilometers</option>
									</select>
								</div>
							</div>
						</div>
						<button id="saveMap" class="pull-right" data-dismiss="modal">ok</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
